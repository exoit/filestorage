﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.Models;

namespace WpfApp1.Database
{
    public class SqLiteContext : DbContext 
    {
        public DbSet<File> Files { get; set; }

        private static string mConnectionString = "Data Source={0};Version={1};FailIfMissing=True;";

        public SqLiteContext(string databasePath, string databaseVersion)
            : base(new SQLiteConnection(string.Format(mConnectionString, databasePath, databaseVersion)), true)
        {

        }

        /// <summary>
        /// Bad idea not check through model 0_x
        /// </summary>
        /// <param name="databasePath"></param>
        /// <param name="databaseVersion"></param>
        public static void SqLiteCheck(string databasePath, string databaseVersion)
        {
            if (!System.IO.File.Exists(databasePath))
                throw new Exception( $"File opened \"{databasePath}\" that is not a database file");

            var conn = new SQLiteConnection(string.Format(mConnectionString, databasePath, databaseVersion));

            try
            {
                conn.Open();

                var cmd = new SQLiteCommand(
                    "select id, name, type, text, createdAt, size, compressedSize from files limit 1",
                    conn);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("Error database:\n" + ex.Message);
            }
            finally
            {
                conn.Dispose();
            }
        }
    }
}
