﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Models;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for FileBrowser.xaml
    /// </summary>
    public partial class FileBrowser : Window
    {
        private readonly File mFile;

        public FileBrowser(File file)
        {
            InitializeComponent();
            mFile = file;

            Owner = Application.Current.MainWindow;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var rawText = mFile.DecompressionString();
            string formattingText = null;

            try
            {
                if (mFile.Type == FileType.Json)
                    formattingText = FileManager.PrintJson(rawText);

                if (mFile.Type == FileType.Xml)
                    formattingText = FileManager.PrintXml(rawText);

                FileText.Document.Blocks.Clear();
                FileText.Document.Blocks.Add(new Paragraph(new Run(formattingText ?? rawText)));

                SetTextHighlight(mFile.Type, formattingText ?? rawText);
            }
            catch (Exception ex)
            {
                var box = MessageBox.Show("Document load error:\n" + ex.Message, "Message", MessageBoxButton.OK,
                    MessageBoxImage.Error);

                if (box == MessageBoxResult.OK)
                    Close();
            }
        }

        private void SetTextHighlight(FileType type, string text)
        {
            var jsonPattern = new Regex("\"([a-zA-Z0-9_]+)\":");
            var xmlPattern = new Regex("(<(/)?[a-zA-Z]+>)");

            var start = FileText.Document.ContentStart;

            if (type == FileType.Json)
            {
                var matches = jsonPattern.Matches(text);

                foreach (Match item in matches)
                {
                    var itemStart = GetPoint(start, item.Index);
                    var itemEnd = GetPoint(itemStart, item.Length);

                    //Console.WriteLine($"Item: {item.Value}, Start: {item.Index}, End: {item.Index + item.Length}");

                    FileText.Selection.Select(itemStart, itemEnd);
                    FileText.Selection.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold);
                }
            }

            if (type == FileType.Xml)
            {
                var matches = xmlPattern.Matches(text);

                foreach (Match item in matches)
                {
                    var itemStart = GetPoint(start, item.Index);
                    var itemEnd = GetPoint(itemStart, item.Length);

                    //Console.WriteLine($"Item: {item.Value}, Start: {item.Index}, End: {item.Index + item.Length}");

                    FileText.Selection.Select(itemStart, itemEnd);
                    FileText.Selection.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold);
                }
            }
        }

        private TextPointer GetPoint(TextPointer start, int pos)
        {
            var ret = start;
            int i = 0;

            while (i < pos)
            {
                if (ret.GetPointerContext(LogicalDirection.Forward) == TextPointerContext.Text) i++;
                if (ret.GetPositionAtOffset(1, LogicalDirection.Forward) == null) return ret;
                ret = ret.GetPositionAtOffset(1, LogicalDirection.Forward);
            }

            return ret;
        }
    }
}
