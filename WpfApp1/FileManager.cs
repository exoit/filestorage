﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;
using WpfApp1.Models;
using File = System.IO.File;
using Formatting = System.Xml.Formatting;

namespace WpfApp1
{
    public static class FileManager
    {
        public static Models.File ReadFile(string path)
        {
            var text = File.ReadAllText(path);
            var fileName = Path.GetFileName(path);

            text = new Regex("[\t\r\n]").Replace(text, "");

            var file = new Models.File
            {
                Name = fileName,
                Type = GetFileType(fileName)
            };

            file.CompressionString(text);

            return file;
        }

        private static FileType GetFileType(string fileName)
        {
            var items = fileName.Split('.');
            var extension = items.LastOrDefault();

            if (extension != null)
            {
                extension = extension.ToLowerInvariant();
                extension = extension.Trim();

                switch (extension)
                {
                    case "json":
                        return FileType.Json;

                    case "xml":
                        return FileType.Xml;

                    case "txt":
                        return FileType.Txt;
                }
            }

            return FileType.Unknown;
        }

        public static string PrintXml(string xml)
        {
            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
            XmlDocument document = new XmlDocument();
            document.LoadXml(xml);

            writer.Formatting = Formatting.Indented;

            document.WriteContentTo(writer);
            writer.Flush();
            mStream.Flush();

            mStream.Position = 0;

            StreamReader sReader = new StreamReader(mStream);

            return sReader.ReadToEnd();
        }

        public static string PrintJson(string json)
        {
            var obj = JsonConvert.DeserializeObject(json, new JsonSerializerSettings
            {
                //Error = (se, ev) => { ev.ErrorContext.Handled = true; }
            });

            return JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented);
        }
    }
}
