﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Media3D;
using Microsoft.Win32;
using WpfApp1.Database;
using WpfApp1.Models;
using WpfApp1.Properties;
using File = WpfApp1.Models.File;

namespace WpfApp1.ViewModels
{
    public class FileView : INotifyPropertyChanged
    {
        private SqLiteContext mDb;
        private IEnumerable<File> mFiles;

        private Processor mAdd;
        private Processor mDelete;
        private Processor mShow;
        private Processor mSaveAs;

        public IEnumerable<File> Files
        {
            get { return mFiles; }
            set
            {
                mFiles = value;
                OnPropertyChanged();
            }
        }

        public FileView()
        {
            mDb = new SqLiteContext(Settings.Default.DATABASE_PATH,
                Settings.Default.DATABASE_VERSION);

            mDb.Files.Load();
            Files = mDb.Files.Local.ToBindingList();
        }

        public Processor Add => mAdd ?? (mAdd = new Processor(async a =>
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                CheckFileExists = true,
                AddExtension = true,
                Multiselect = false,
                Filter = "Text files (*.json;*.xml;*.txt)|*.json;*.xml;*.txt"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                var filePath = openFileDialog.FileName;
                var fileName = Path.GetFileName(filePath);

                var dlg = new DialogBox(fileName);

                if (dlg.ShowDialog() == true)
                {
                    var file = FileManager.ReadFile(filePath);
                    file.Name = dlg.TextBox.Text;

                    mDb.Files.Add(file);
                    await mDb.SaveChangesAsync();
                }
            }

        }));

        public Processor Delete => mDelete ?? (mDelete = new Processor(async a =>
        {
            if (a == null) return;

            var file = a as File;

            mDb.Files.Remove(file);

            await mDb.SaveChangesAsync();
        }));

        public Processor Show => mShow ?? (mShow = new Processor(async a =>
        {
            if (a == null) return;

            var file = a as File;

            new FileBrowser(file).Show();
        }));

        public Processor SaveAs => mSaveAs ?? (mSaveAs = new Processor(async a =>
        {
            if (a == null) return;

            var file = a as File;

            SaveFileDialog saveFileDialog1 = new SaveFileDialog
            {
                FileName = file.Name
            };

            if (saveFileDialog1.ShowDialog() == true)
                System.IO.File.WriteAllText(saveFileDialog1.FileName, file.DecompressionString());
        }));

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
