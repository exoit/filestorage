﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp1
{
    public class Processor : IAsyncCommand
    {
        private bool mIsExecuting;
        private readonly Func<object, Task> mExecute;
        private readonly Func<bool> mCanExecute;

        public Processor(Func<object, Task> execute, Func<bool> canExecute = null)
        {
            mExecute = execute;
            mCanExecute = canExecute;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute()
        {
            return !mIsExecuting && (mCanExecute?.Invoke() ?? true);
        }

        public async Task ExecuteAsync(object parameter)
        {
            if (CanExecute())
            {
                try
                {
                    mIsExecuting = true;
                    await mExecute(parameter);
                }
                finally
                {
                    mIsExecuting = false;
                }
            }

            RaiseCanExecuteChanged();
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute();
        }

        void ICommand.Execute(object parameter)
        {
            ExecuteAsync(parameter).FireAsync();
        }
    }

    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync(object parameter);

        bool CanExecute();
    }

    public static class Tools
    {
        public static async void FireAsync(this Task task)
        {
            try
            {
                await task;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException();
            }
        }
    }
}
