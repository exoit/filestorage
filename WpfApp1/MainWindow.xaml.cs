﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp1.Database;
using WpfApp1.Models;
using WpfApp1.ViewModels;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                SqLiteContext.SqLiteCheck(Properties.Settings.Default.DATABASE_PATH,
                    Properties.Settings.Default.DATABASE_VERSION);

                DataContext = new FileView();
            }
            catch (Exception ex)
            {
                var box = MessageBox.Show(ex.Message, "Message", MessageBoxButton.OK, MessageBoxImage.Error);

                if (box == MessageBoxResult.OK)
                    Close();
            }
        }
    }
}
