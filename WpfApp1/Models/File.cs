﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Models
{
    [Table("files")]
    public class File
    {
        [Column("id")]
        [Key]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("type")]
        public FileType Type { get; set; }

        [Column("text")]
        public string Text { get; set; }

        [Column("createdAt")]
        public string CreatedAt { get; set; } = DateTime.Now.ToString();

        [Column("size")]
        public int Size { get; set; }

        [Column("compressedSize")]
        public int CompressedSize { get; set; }

        public void CompressionString(string str)
        {
            Size = str.Length;

            var inBytes = Encoding.UTF8.GetBytes(str);

            using (var stream = new MemoryStream())
            {
                using (var gZipStream = new GZipStream(stream, CompressionMode.Compress))
                    gZipStream.Write(inBytes, 0, inBytes.Length);

                var resultBytes = stream.ToArray();
                var resultBase64 = Convert.ToBase64String(resultBytes);

                CompressedSize = resultBase64.Length;
                Text = resultBase64;
            }
        }

        public string DecompressionString()
        {
            if (Text == null)
                throw new NotImplementedException();

            var inBytes = Convert.FromBase64String(Text);

            using (var stream = new MemoryStream(inBytes))
            using (var gZipStream = new GZipStream(stream, CompressionMode.Decompress))
            using (var reader = new StreamReader(gZipStream))
            {
                return reader.ReadToEnd();
            }
        }
    }

    public enum FileType
    {
        Unknown = 0,
        Json = 1,
        Xml  = 2,
        Txt  = 3
    }
}
